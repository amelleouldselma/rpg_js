export default class Battle {

    laPartie;
    leHeros;
    lEnnemi;
    leGagnant;
    lePerdant;

    constructor(leHeros, lEnnemi) {
        this.leHeros = leHeros;
        this.lEnnemi = lEnnemi;

    }

    start() {

        // La partie commence
        this.laPartie = true; 

        //Top départ
        this.begin(); 


       //Si la santé de l'ennemie est sup ou egale a 0 on continue le combat
        if (this.lEnnemi.health >= 0) {          
            this.deuxièmeAttack();
            console.log(this.deuxièmeAttack())

            // Deuxieme attaque
            if (this.leHeros.health >= 0) {      
                this.begin();

            } 
            
            //Si la santé est inf ou egale a 0 le heros est mort, ennemi gagnant
            else {                              
                console.log("Le Gagnant est:" + this.lEnnemi.name)  
                this.leHeros.die();
            }

            } 

            //Si la santé est inf ou egale a 0 l'ennemi est mort, heros gagnant
            else {                              
                console.log("Le Gagnant est:" + this.leHeros.name)
                this.lEnnemi.die();
            }

        this.leGagnant = this.lEnnemi;

        this.lePerdant = this.leHeros;

        //Fin de la partie
        this.status = false;





    }

// Le debut du combat : le heros attaque en premier

    begin() {
        this.leHeros.attack(this.lEnnemi);
        console.log("Le Héros attaque")
        console.log(this.leHeros.attack(this.lEnnemi));
        this.lEnnemi.health = this.leHeros.attack(this.lEnnemi);
        console.log(this.lEnnemi.health);
        // le heros qui commence

    }

//Seconde attaque : l'ennemie repond au repond par une attaque
    deuxièmeAttack() {

        this.lEnnemi.attack(this.leHeros);
        console.log("L'Ennemi attaque")
        console.log(this.lEnnemi.attack(this.leHeros));
        this.leHeros.health = this.lEnnemi.attack(this.leHeros);
        console.log(this.leHeros.health);
    }

    theEnd(leGagnant, lePerdant) {
        //Winner gagne + 2 xp
        leGagnant.setXp(2);
        //Winner gagne +10% de la vie de l'ennemi (heal)
        let newTempHealth = leGagnant.tempHealth + (lePerdant.health * 1.1);
        leGagnant.setTempHealth(newTempHealth);
        leGagnant.levelUp();
    }
    

}



//On débute par lancer une partie +
//On choisi le héros
//on choisi l'ennemi ou on fait un random
//On créé un combat entre les deux personnages choisi +
//Fin de partie suite a la mort de l'un d'entre eux +
//Afficher les statistiques
//Compter les xp -> lvl augmente ou pas +
