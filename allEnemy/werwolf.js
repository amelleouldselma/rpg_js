import Enemy from './Enemy.js'

export default class Werwolf extends Enemy{
    constructor(name){
        super(name)
        this.health = 1.5;
        
    }
    attack(cible) {
        //Resistance de 50%
        return cible.hitStrength -= 0.5 * cible.hitStrength;
    }

}