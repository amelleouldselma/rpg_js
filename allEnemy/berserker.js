import Enemy from './Enemy.js'

export default class Berserker extends Enemy {
    constructor(name) {
        super(name)

    }

    getAttacked(cible) {
        //Resistance de 30%
        return cible.hitStrength -= 0.3 * cible.hitStrength;
    }
}