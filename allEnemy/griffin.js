import Enemy from './Enemy.js'

export default class Griffin extends Enemy{
    constructor(name){
        super(name)

    }
    //Griffin ont une methode fly et attackFromSky supplementaires qui augmentent de 10% 
    //la resistance et 10% l'attaque. Leur attaques devront suivre la logique suivante :attaque au sol
    //fly, attackFromSky

    attackground(){


    }

    fly(){ //augmentent de 10% la resistance
        this.hitStrength *= 1.1;

    }

    attackFromSky(){ //augmentent de 10%  l'attaque
        this.attack *= 1.1;
    }

}