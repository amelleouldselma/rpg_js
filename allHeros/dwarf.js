import Heros from './Heros.js'

export default class Dwarf extends Heros {
    constructor(name, race) {
        super(name, Dwarf)

    }
    race = "dwarf";


    getAttacked(adversaire) {
        //Génère un nombre flottant entre 0 et 1
        let chance = Math.random();
        //Exécute le code dans 20% des cas
        if (chance <= 0.2) {
            //Réduit les dommages de l'ennemi de 50%
            return adversaire.hitStrength -= 0.5 * adversaire.hitStrength;
        }
    }
}