export default class Heros {
    constructor(name, race) {
        this.name = name;
        this.health = 100;
        this.hitStrength = 30;
        this.lvl = 1;
        this.xp = 0;
        this.race = race;


    }
    getName() {
        return this.name;
    }
    setName(name) {
        this.name;
    }
    getHealth() {
        return this.health;
    }
    setHealth(health) {
        this.health;
    }
    getHitStrenght() {
        return this.hitStrenght;
    }
    setHitStrenght(hitStrength) {
        this.hitStrength;

    }
    getXp() {
        return this.xp;
    }
    setXp(xp) {
        this.xp;

    }
    getLvl() {
        return this.lvl;

    }
    setLvl(lvl) {
        this.lvl;

    }
    attack(rival) {
        return rival.health -= this.hitStrength;
    }
    die() {
        if (this.health <= 0) {
            return true;
        } else {
            return false;
        }
    }
    levelUp() {
        if (this.xp == 10) {
            //chaque combat gagné rapporte 2 de xp
            this.xp = 10;
            this.lvl++;
            //quand l'xp atteint 10 le lvl incremente de 1
            //on recupere 10% de l'health de l'ennemi
            this.health += 10;
            this.hitStrength += 5;
        }
    }

}