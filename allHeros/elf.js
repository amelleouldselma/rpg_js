import Heros from './Heros.js'

export default class Elf extends Heros {
    constructor(name, race) {
        super(name, Elf)
    }


    setHitStrength(adversaire) {
        if (adversaire.attackFromSky()) {
            //Applique le bonus de classe +10% hitStrength
            return this.hitStrength += this.hitStrength * 0.1;
        }
        if (adversaire.attack()) {
            //Applique le malus de classe -10% hitStrength
            return this.hitStrength -= this.hitStrength * 0.1;
        }
    }

}