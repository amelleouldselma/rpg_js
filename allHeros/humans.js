import Heros from './Heros.js'

export default class Humans extends Heros{
    constructor(name, race){
        super(name, Humans)
        this.hitStrength *= 1.1;
        
        
    }
    attack(adversaire){
        const newHealth = adversaire.getHealth() - this.hitStrength;
        adversaire.setHealth(newHealth);
    }
}

//Humans : +10% de hitStrength sur les ennemis au sol, -10% de hitStrength sur les ennemis volants