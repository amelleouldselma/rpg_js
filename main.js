import Assassin from './allEnemy/assassin.js'
import Berserker from './allEnemy/berserker.js'
import Dragon from './allEnemy/dragon.js'
import Golem from './allEnemy/golem.js'
import Griffin from './allEnemy/griffin.js'
import Werwolf from './allEnemy/werwolf.js'


import Dwarf from './allHeros/dwarf.js';
import Elf from './allHeros/elf.js';
import Humans from './allHeros/humans.js';

import BattleSimulation from './battleFolder/battleSimulation.js';
import Battle from './battleFolder/battle.js';



let newDwarf = new Dwarf('Orel');
console.log(newDwarf.getName());

let elfFredo = new Elf("Fredo");
console.log(elfFredo.getName());

let fight1 = new Battle(newDwarf, elfFredo)
console.log(fight1);

fight1.begin();
fight1.deuxièmeAttack();
fight1.start();




let DragonBob = new Dragon("Bob");
console.log(DragonBob.getName());

let HumansEric = new Humans('Eric');
console.log(HumansEric.getName());

let fight2 = new Battle(DragonBob, HumansEric)
console.log(fight2);

fight2.begin();
fight2.deuxièmeAttack();
fight2.start();